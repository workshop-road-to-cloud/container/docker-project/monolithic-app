FROM tomcat:9-jre8

MAINTAINER Warren Roque <wroquem@gmail.com>

COPY app-challenge.war /usr/local/tomcat/webapps
